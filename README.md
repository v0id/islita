# islita

pequeñas islitas de música

_inspirado en [lovelikeasunset](https://phoenixmania.github.io/)_

### [articulo explicando](https://void.partidopirata.com.ar/islita.html)

## islitas <3

- [void-radio](https://murciela.ga)

creaste tu islita? mandame un mensaje!

## uso

instalas islita:

```bash
npm i git+https://0xacab.org/v0idifier/islita -g
```

creas tu islita:

```bash
islita crear mi-isla
```

compilas tu islita:

```bash
cd mi-isla
npm run compilar
```

## acelerar descargas

las descargas son bastante lentas por defecto. para acelerarlas, podés instalar [youtube-dl](https://youtube-dl.org/).

## servidor mágico

`islita` viene con un "servidor mágico" que te deja agregar canciones junto a un [bookmarklet](https://0xacab.org/snippets/751).
para iniciarlo:

```bash
npm run servidor-magico
```

## playlists (.m3u8)

`islita` genera una `playlist.m3u8` automáticamente para ser usada con cualquier reproductor de musica decente. lamentablemente, esta playlist tiene que ser cargada y interpretada de una forma especifica para que funcione, ya que no especifica la dirección de la web con la música. para que sea más compatible, podés agregar esto a tu `config.yml`:

```yaml
baseUrl: https://URL.DE.MI.ISLA/
```

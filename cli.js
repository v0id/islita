#!/usr/bin/env node

const fs = require('fs-extra')
const exec = require('mz/child_process').exec
const readline = require('readline')
const path = require('path')
const yaml = require('js-yaml')
const Handlebars = require('handlebars')
const ytdl = require('ytdl-core')
const express = require('express')
const [,, ...params] = process.argv

function streamPromise(stream) {
  return new Promise((resolve, reject) => {
    stream.on('end', () => {
      resolve('end')
    })
    stream.on('finish', () => {
      resolve('finish')
    })
    stream.on('error', error => {
      reject(error)
    })
  })
}

function help () {
  console.log(
`islitas multimedia <3
hecho con amor por void

comandos:
  islita crear [nombre de directorio]
    crea una islita en el directorio elegido. si no existe, lo crea. si no se especifica, se usa el directorio atual.
  
  islita compilar
    compila la islita en el directorio actual.`
  )
  process.exit(0)
}

if (params.length < 1) {
  console.error('tenés que especificar un comando!')
  help()
}

const preguntar = question => new Promise((resolve, reject) => {
  const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
  })

  rl.question(question, answer => {
    resolve(answer)
    rl.close()
  })
})

async function detectarEmpaquetador (directorio) {
  // pnpm
  try {
    const [version] = await exec('pnpm --version')
    if (/^\d+.\d+.\d+\n$/.test(version)) {
      return 'pnpm'
    }
  } catch (e) {}
  // yarn
  try {
    const [version] = await exec('yarn --version')
    if (/^\d+.\d+.\d+\n$/.test(version)) {
      return 'yarn'
    }
  } catch (e) {}
  // npm
  try {
    const [version] = await exec('npm --version')
    if (/^\d+.\d+.\d+\n$/.test(version)) {
      return 'npm'
    }
  } catch (e) {}

  return 'desconocido'
}

async function detectarYoutubeDl () {
  try {
    const [version] = await exec('youtube-dl --version')
    if (/^\d+.\d+.\d+\n$/.test(version)) {
      return true
    }
  } catch (e) {}
  return false
}

function instalarDependencias (directorio, empaquetador) {
  switch (empaquetador) {
    case 'pnpm':
    case 'npm':
      return exec(`cd ${path.resolve('.', directorio)} && ${empaquetador} install`)
    // yarn se cree muy copado por romper APIs
    case 'yarn':
      return exec(`cd ${path.resolve('.', directorio)} && yarn`)
    default:
      throw new Error(`no se pudo instalar con el empaquetador ${empaquetador}`)
  }
}

async function crearIslita (directorio = '.', ...params) {
  console.log(`creando islita en \`${directorio}\` :D:D`)
  const dir = path.join(directorio)
  const yaExiste = await fs.exists(dir) && await fs.stat(dir)

  if (yaExiste) {
    console.warn(`hm.. ${dir} ya existe. ¿estás segur@ que querés crear una isla ahí? puede que se sobreescriban cosas.`)
    const res = await preguntar(`respuesta [s/n]> `)
    if (/^(si?|y(es)?)$/.test(res)) {
      console.log('okay, lo voy a sobreescribir...')
    } else {
      console.error('anulando creación.')
      process.exit(1)
    }
  }

  if (!yaExiste || !yaExiste.isDirectory()) {
    await fs.mkdir(directorio)
  }

  // archivo de configuración principal (config.yml)
  let config = (await fs.readFile(path.join(__dirname, './plantillas/config.yml'))).toString()
  config = config.replace('###NOMBRE###', path.basename(directorio))
  await fs.writeFileSync(path.join(directorio, 'config.yml'), config)

  // archivo de configuración de npm (package.json)
  let packageJson = (await fs.readFile(path.join(__dirname, './plantillas/package.json'))).toString()
  packageJson = packageJson.replace('###NOMBRE###', path.basename(directorio))
  await fs.writeFileSync(path.join(directorio, 'package.json'), packageJson)

  // archivos de canciones
  const cancionesDir = path.join(directorio, 'canciones')
  if (!(await fs.exists(cancionesDir) && (await fs.stat(cancionesDir)).isDirectory())) {
    await fs.mkdir(cancionesDir)
  }

  let cancion = (await fs.readFile(path.join(__dirname, './plantillas/cancion.yml'))).toString()
  await fs.writeFileSync(path.join(directorio, 'canciones/the-love-club.yml'), cancion)

  // la web
  const webDir = path.join(directorio, 'web')
  if (!(await fs.exists(webDir) && (await fs.stat(webDir)).isDirectory())) {
    await fs.mkdir(webDir)
  }

  let indexHbs = (await fs.readFile(path.join(__dirname, './plantillas/index.hbs'))).toString()
  await fs.writeFile(path.join(directorio, 'web/index.hbs'), indexHbs)

  let magiaJs = (await fs.readFile(path.join(__dirname, './plantillas/magia.js'))).toString()
  await fs.writeFile(path.join(directorio, 'web/magia.js'), magiaJs)

  let estilosCss = (await fs.readFile(path.join(__dirname, './plantillas/estilos.css'))).toString()
  await fs.writeFile(path.join(directorio, 'web/estilos.css'), estilosCss)

  const empaquetador = await detectarEmpaquetador()

  // linkear en vez de instalar
  if (params.find(param => param === '--link')) {
    console.log(`linkeando con ${empaquetador}...`)
    await exec(`cd ${directorio} && ${empaquetador} link islita`)
  } else {
    console.log(`voy a instalar los paquetes necesarios con ${empaquetador}...`)
    await instalarDependencias(directorio, empaquetador)
  }

  console.log(`creada islita en ${directorio}! <3`)
}

async function generarPlaylist ({ config, canciones }) {
  let playlist = '#EXTM3U\n'
  playlist += `#PLAYLIST:${config.nombre}\n`
  for (const { titulo, artista, url } of canciones) {
    playlist += `#EXTINF:0,${artista} - ${titulo}\n`
    playlist += (config.baseUrl || '') + url + '\n'
  }
  await fs.writeFile('output/playlist.m3u8', playlist)
}

async function compilarIslita () {
  let config
  try {
    config = yaml.safeLoad(await fs.readFile('config.yml', 'utf8'))
  } catch (error) {
    console.error('no se pudo abrir el archivo de configuración config.yml :c')
    process.exit(1)
  }

  if (!(await fs.exists('canciones') && (await fs.stat('canciones')).isDirectory())) {
    console.error('no se pudo encontrar el directorio `canciones`. :c')
    process.exit(1)
  }

  if (!(await fs.exists('web') && (await fs.stat('web')).isDirectory())) {
    console.error('no se pudo encontrar el directorio `web`. :c')
    process.exit(1)
  }

  await fs.ensureDir('output')
  await fs.copy('web', 'output', {
    filter: (src, dest) => !/\.hbs$/.test(src),
  })
  await fs.ensureDir('output/canciones')

  const archivosDeCanciones = await fs.readdir('canciones')
  let canciones = []

  const tieneYoutubeDl = await detectarYoutubeDl()
  if (tieneYoutubeDl) {
    console.log('youtube-dl detectado! las descargas van a ser más rápidas ;)')
  }

  for (const archivo of archivosDeCanciones) {
    const cancion = yaml.safeLoad(await fs.readFile(path.join('canciones', archivo), 'utf8'))

    const outputExiste = (await fs.readdir('output/canciones')).find(a => {
      const [, match] = a.match(/(.+)\.(.+)$/)
      return match === archivo.replace('.yml', '')
    })

    let nombreOutput
    if (!outputExiste) {
      console.log(`descargando ${cancion.titulo} de ${cancion.artista}...`)
      let info
      try {
        info = await ytdl.getInfo(cancion.fuentes[0])
      } catch (error) {
        console.log('error descargando la canción! fuente:', cancion.fuentes[0], 'error:', error)
        throw 'error!'
      }
      const preset = {
        quality: 'highestaudio',
        filter: 'audioonly',
      }
      const format = ytdl.chooseFormat(info.formats, preset)

      nombreOutput = path.parse(archivo).name + '.downloading.' + format.container
      const output = path.join('output/canciones', nombreOutput)

      if (tieneYoutubeDl) {
        await exec(`youtube-dl --no-mtime -o "${output}" --format ${format.itag} "${cancion.fuentes[0]}"`)
      } else {
        await streamPromise(ytdl.downloadFromInfo(info, preset)
          .pipe(fs.createWriteStream(output)))
      }

      nombreOutput = path.parse(archivo).name + '.' + format.container
      await fs.move(output, path.join('output/canciones', nombreOutput))
    }

    canciones.push({
      ...cancion,
      url: 'canciones/' + (outputExiste || nombreOutput),
    })
  }

  const data = { config, canciones, inyectarScripts: '' }

  await generarPlaylist(data)

  if (config.ipfs) {
    console.log('generando ipfs...')
    const ipfsHash = (await exec(`ipfs add output -r -q | tail -n 1`))[0].trim()
    console.log(ipfsHash)
    await fs.copy(path.join(__dirname, 'plantillas/ipfs-0.35.0.min.js'), 'output/ipfs-0.35.0.min.js')
    await fs.copy(path.join(__dirname, 'plantillas/videostream-3.2.0.js'), 'output/videostream-3.2.0.js')
    data.inyectarScripts = `
    <script src="ipfs-0.35.0.min.js"></script>
    <script src="videostream-3.2.0.js"></script>
    <script>
      const node = new window.Ipfs()
      let stream

      const promise = new Promise((resolve, reject) => {
        node.on('ready', resolve)
      })

      window.setearAudio = async (audioEl, path) => {
        await promise
        new videostream({
          createReadStream: function createReadStream (opts) {
            const start = opts.start
            const end = opts.end ? start + opts.end + 1 : undefined
            console.log(\`Stream: Asked for data starting at byte \${start} and ending at byte \${end}\`)

            if (stream && stream.destroy) {
              stream.destroy()
            }

            stream = node.catReadableStream('${ipfsHash}/' + path, {
              offset: start,
              length: end && end - start
            })

            stream.on('error', error => console.error(error))

            if (start === 0) {
              console.log('esperando...')
            }

            return stream
          }
        }, audioEl)
      }
    </script>
    `
  }

  const template = Handlebars.compile((await fs.readFile('web/index.hbs', 'utf8')).toString())
  const html = template(data)
  await fs.writeFile('output/index.html', html)

  let magiaJs = (await fs.readFile('web/magia.js', 'utf8')).toString()
  magiaJs = magiaJs.replace('###INYECTAR DATA###', JSON.stringify(data))
  await fs.writeFile('output/magia.js', magiaJs)

  console.log(`${config.nombre} ha sido compilada! <3`)
}

function servidorMagico (...params) {
  console.log('iniciando servidor...')
  const app = express()

  app.get('/cancion/:nombreDeArchivo', async (req, res) => {
    await fs.writeFile(`canciones/${req.params.nombreDeArchivo}.yml`,
`titulo: ${req.query.titulo}
artista: ${req.query.artista}
fuentes:
  - ${req.query.youtubeUrl}`)

    console.log(`agregé ${req.query.titulo} de ${req.query.artista} (${req.query.youtubeUrl}) a ${req.params.nombreDeArchivo}`)
    res.json({ success: true })
  })

  const PORT = process.env.PORT || 1337
  app.listen(PORT, () => {
    console.log(`iniciado en localhost:${PORT}`)
  })
}

switch (params[0]) {
  case 'crear':
    crearIslita(params[1], ...params)
    break
  case 'compilar':
    compilarIslita(...params)
    break
  case 'servidor-magico':
    servidorMagico(...params)
    break
  default:
    console.error('no conozco ese comando!')
    help()
}

window.DATA = ###INYECTAR DATA###

const s = s => document.querySelector(s)
const ss = s => document.querySelectorAll(s)

window.setearAudio = window.setearAudio || function (audioEl, path) {
  audioEl.src = path
}

let escuchandoIndex

async function play (index) {
  escuchandoIndex = index
  const cancion = DATA.canciones[index]

  if (!cancion) {
    for (const el of ss('.lista li')) {
      el.classList.remove('playing')
    }
    s('#playing').textContent = `nada.`
    s('audio').src = ''
    return
  }

  await window.setearAudio(s('audio'), cancion.url)

  s('#playing').textContent = `${cancion.titulo} de ${cancion.artista}`
  for (const el of ss('.lista li')) {
    el.classList.remove('playing')
  }
  s(`.lista li[data-i="${index}"]`).classList.add('playing')
  s('audio').play()
}

window.onload = () => {
  play(0)
  s('audio').addEventListener('ended', event => {
    play(escuchandoIndex + 1)
  })
}
